package com.felephantflame.seetafacedemo;

import com.felephantflame.seetafacedemo.page.IndexPage;
import javafx.application.Application;

/**
 * @author puye(0303)
 */
public class MainApplication  {

    public static void main(String[] args) {
        Application.launch(IndexPage.class);
    }
}
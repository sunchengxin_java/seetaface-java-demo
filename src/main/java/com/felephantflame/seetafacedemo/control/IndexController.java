package com.felephantflame.seetafacedemo.control;
import com.felephantflame.seetafacedemo.model.FaceModel;
import com.felephantflame.seetafacedemo.service.OpenCvFrameService;
import com.felephantflame.seetafacedemo.service.impl.SeetaFaceFrameServiceImpl;
import com.seeta.sdk.EyeStateDetector;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author puye(0303)
 * @since 2023/3/6
 */
public class IndexController {

    private static final Logger log = LoggerFactory.getLogger(IndexController.class);

    @FXML
    public Button exportData;

    @FXML
    public Button startTask;

    @FXML
    public Canvas centerView;

    @FXML
    public Label frameCount;

    private double oldStageX;

    private double oldStageY;

    private double oldScreenX;

    private double oldScreenY;

    @FXML
    public Label frameFps;

    private OpenCvFrameService openCvFrameService;

    @FXML
    public Region close;

    @FXML
    public Label leftEye;

    @FXML
    public Label facePoseData;

    @FXML
    public Label mask;

    @FXML
    public Label faceNumber;

    @FXML
    public Label sex;

    @FXML
    public Label rightEye;

    @FXML
    public Label cameraStatus;

    @FXML
    public Label cameraStatusStr;

    @FXML
    public Label poseResult;

    @FXML
    public Label cameraSource;

    @FXML
    public Label age;

    @FXML
    public AnchorPane top;

    @FXML
    public void initialize(){
        top.setOnMousePressed(mouseEvent -> {
            Stage primaryStage = getStage();
            oldStageX = primaryStage.getX();
            oldStageY = primaryStage.getY();
            oldScreenX = mouseEvent.getScreenX();
            oldScreenY = mouseEvent.getScreenY();
        });
        top.setOnMouseDragged(mouseEvent -> {
            Stage primaryStage = getStage();
            primaryStage.setX(mouseEvent.getScreenX() - oldScreenX + oldStageX);
            primaryStage.setY(mouseEvent.getScreenY() - oldScreenY + oldStageY);
        });
        close.setOnMouseClicked(mouseEvent -> System.exit(-1));
        openCvFrameService = new SeetaFaceFrameServiceImpl();
        // true : 当前为已启动人脸任务状态 false 当前为已关闭人脸任务状态
    }


    @FXML
    public void getOnCollect() {
        // 开始人脸采集
        Platform.runLater(() -> {
            cameraStatus.setStyle("-fx-background-color: rgb(240,167,50);-fx-background-radius: 50;-fx-border-radius: 50");
            cameraStatusStr.setText("人脸采集启动中");
        });
        int fps = 1000 / 40;
        log.info("软件启动中");
        Canvas canvas = new Canvas(500, 500);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        AnchorPane root = (AnchorPane) centerView.getParent();
        root.getChildren().add(canvas);
        Service<OpenCVFrameGrabber> service = new Service<>() {
            @Override
            protected Task<OpenCVFrameGrabber> createTask() {
                return new Task<>() {
                    @Override
                    protected OpenCVFrameGrabber call() throws Exception {
                        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
                        grabber.setImageWidth(500);
                        grabber.setImageHeight(500);
                        grabber.start();
                        log.info("软件已启动");
                        return grabber;
                    }
                };
            }
        };
        service.setOnSucceeded(event -> {
            cameraStatusStr.setText("已启动人脸采集");
            cameraStatus.setStyle("-fx-background-color: #0aee0a;-fx-background-radius: 50;-fx-border-radius: 50");
            OpenCVFrameGrabber grabber = (OpenCVFrameGrabber) event.getSource().getValue();
            Service<Void> drawService = new Service<>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<>() {
                        @Override
                        protected Void call() throws Exception {
                            while (!isCancelled()) {
                                // 缓存数据
                                FaceModel faceModel = resolveImageData(grabber.grab());
                                if (Objects.nonNull(faceModel)) {
                                    faceModel.setCameraSource("Camera 0");
                                    Platform.runLater(() -> {
                                        frameFps.setText(String.valueOf(grabber.getFrameRate()));
                                        age.setText(String.valueOf(faceModel.getAges()[0]));
                                        leftEye.setText(eyeStatusStr(faceModel.getEyeStatus()[0]));
                                        rightEye.setText(eyeStatusStr(faceModel.getEyeStatus()[1]));
                                        gc.drawImage(faceModel.getFxImageData(), 0, 0);
                                        sex.setText(faceModel.getGender());
                                        mask.setText(faceModel.isWearMaskOrNot()?"已佩戴口罩":"未佩戴口罩");
                                        frameCount.setText(String.valueOf(faceModel.getFrameCount()));
                                        faceNumber.setText(faceModel.getFaceTotal() +"个");
                                        facePoseData.setText(String.format("向量数据为：X-%s-Y-%s-Z-%s",faceModel.getYaw()[0],
                                                faceModel.getPitch()[0],faceModel.getRoll()[0]));
                                        poseResult.setText(faceModel.getPoseResult());
                                        cameraSource.setText(faceModel.getCameraSource());
                                    });
                                    TimeUnit.MILLISECONDS.sleep(fps);
                                } else {
                                    log.error("当前帧数据返回有误！");
                                }
                            }
                            return null;
                        }
                    };
                }
            };
            drawService.start();
        });
        service.start();
    }

    private FaceModel resolveImageData(Frame frame) {
        Java2DFrameConverter converter = new Java2DFrameConverter();
        // 将BufferedImage转换为JavaFX的Image对象
        BufferedImage bufferedImage = converter.convert(frame);
        FaceModel faceModel = FaceModel.builder().faceImage(bufferedImage).build();
        // 执行人脸识别操作
        try {
            // 首先进行人脸追踪绘制
            faceModel = openCvFrameService.faceTrackAndDetector(faceModel);
            // 其次进行人脸业务识别
            faceModel = openCvFrameService.analyseFaceFrame(faceModel);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        if (Objects.nonNull(faceModel.getFaceImage())) {
            faceModel.setFxImageData(SwingFXUtils.toFXImage(faceModel.getFaceImage(), null));
            return faceModel;
        }
        return null;
    }

    private String eyeStatusStr(EyeStateDetector.EYE_STATE eyeStates){
        switch (eyeStates){
            case EYE_OPEN:
                return "睁开";
            case EYE_CLOSE:
                return "闭合";
            case EYE_RANDOM:
                return "转动";
            case EYE_UNKNOWN:
            default:
                return "未知动作";
        }
    }

    private Stage getStage(){
        return (Stage) centerView.getScene().getWindow();
    }

}

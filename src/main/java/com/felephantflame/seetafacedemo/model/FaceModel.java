package com.felephantflame.seetafacedemo.model;

import com.seeta.sdk.EyeStateDetector;
import javafx.scene.image.Image;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * @author puye(0303)
 * @since 2023/3/8
 */
@Data
@Builder
@Setter
@Getter
@ToString
public class FaceModel implements Serializable {

    private static final long serialVersionUID = 3049394559231656197L;

    private BufferedImage faceImage;

    private BufferedImage compareFaceImage;

    private Boolean isEqual;

    private Image fxImageData;

    private int[] ages;

    private String gender;

    private boolean wearMaskOrNot;

    private long frameCount;

    private Integer faceTotal;

    private float[] yaw;

    private float[] pitch;

    private float[] roll;

    private String poseResult;

    private String cameraSource;

    /**
     * 眼睛状态信息 包含左右眼
     */
    private EyeStateDetector.EYE_STATE[] eyeStatus;

}

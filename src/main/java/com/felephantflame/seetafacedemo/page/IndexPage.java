package com.felephantflame.seetafacedemo.page;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;


/**
 * @author puye(0303)
 * @since 2023/3/6
 */
public class IndexPage extends Application {

    @Override
    public void start(Stage stage)  {
        try {
            FXMLLoader loader = new FXMLLoader(IndexPage.class.getResource("/fxml/indexView.fxml"));
            Parent rootNode = loader.load();
            rootNode.setStyle(" -fx-background-radius: 20; -fx-border-radius: 20;");
            Group group = new Group(rootNode);
            Scene scene = new Scene(group,883,620);
            stage.setScene(scene);
            stage.setTitle("人脸识别测试");
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.felephantflame.seetafacedemo.service;

import com.felephantflame.seetafacedemo.model.FaceModel;

/**
 * @author puye(0303)
 * @since 2023/3/8
 */
public interface OpenCvFrameService {

    /**
     * 人脸追踪与目标检测
     * @param faceModel bufferedImage
     * @return 返回一个Swing的bufferedImage
     */
    FaceModel faceTrackAndDetector(FaceModel faceModel);

    /**
     * 人脸信息识别
     * @param faceModel 传入一个视频帧数据
     * @return 返回一个视频帧分析后的模型
     */
    FaceModel analyseFaceFrame(FaceModel faceModel);

    /**
     * 人脸特征对比
     *
     * @param faceModel bufferedImage compareFaceImage
     * @return 返回判断两个图像是否相等后的模型
     */
    FaceModel faceRecognizerCompare(FaceModel faceModel);

}

package com.felephantflame.seetafacedemo.service.impl;

import com.felephantflame.seetafacedemo.model.FaceModel;
import com.felephantflame.seetafacedemo.service.OpenCvFrameService;
import com.seeta.sdk.AgePredictor;
import com.seeta.sdk.EyeStateDetector;
import com.seeta.sdk.EyeStateDetector.EYE_STATE;
import com.seeta.sdk.FaceDetector;
import com.seeta.sdk.FaceLandmarker;
import com.seeta.sdk.FaceRecognizer;
import com.seeta.sdk.FaceTracker;
import com.seeta.sdk.GenderPredictor;
import com.seeta.sdk.GenderPredictor.GENDER;
import com.seeta.sdk.MaskDetector;
import com.seeta.sdk.QualityOfPoseEx;
import com.seeta.sdk.SeetaDevice;
import com.seeta.sdk.SeetaImageData;
import com.seeta.sdk.SeetaModelSetting;
import com.seeta.sdk.SeetaPointF;
import com.seeta.sdk.SeetaRect;
import com.seeta.sdk.SeetaTrackingFaceInfo;
import com.seeta.sdk.util.LoadNativeCore;
import com.seeta.sdk.util.SeetafaceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author puye(0303)
 * @since 2023/3/8
 */
public class SeetaFaceFrameServiceImpl implements OpenCvFrameService {

    private static final Logger log = LoggerFactory.getLogger(SeetaFaceFrameServiceImpl.class);

    private static final double COMMON_VALUE_0_POINT_5 = 0.5;

    private static final double COMMON_VALUE_0_POINT_2 = 0.2;

    private static final int COMMON_VALUE_0 = 0;

    private static final String BASE_PATH = "D:\\develop\\idea_workspace\\seetaface-java-demo\\src\\main\\resources\\models";

    private static final String PATH = "D:\\develop\\idea_workspace\\seetaface-java-demo\\src\\main\\resources\\models\\face_detector.csta";

    private static final String[] AGE_CSTAS = {BASE_PATH + "/age_predictor.csta"};

    private static final String[] LANDMARKER_CSTAS = {BASE_PATH + "/face_landmarker_pts5.csta"};

    private static final String[] LANDMARKER_CSTAS_68 = {BASE_PATH + "/face_landmarker_pts68.csta"};

    private static final String[] EYE_CSTAS = {BASE_PATH + "/eye_state.csta"};

    private static final String[] MASK_CSTAS = {BASE_PATH + "/mask_detector.csta"};

    private static final String[] GENDER_CSTAS = {BASE_PATH + "/gender_predictor.csta"};

    private static final  String[] POSE_CSTAS = {BASE_PATH + "/pose_estimation.csta"};

    private static final String[] DETECTOR_CSTA = {BASE_PATH + "/face_detector.csta"};

    /**
     * 人脸向量特征提取和对比模型
     */
    private static final String[] RECOGNIZER_CSTA = {BASE_PATH + "/face_recognizer.csta"};

    private static FaceTracker faceTracker;

    private static FaceDetector faceDetector;

    private static AgePredictor agePredictor;

    private static FaceLandmarker faceLandmarker5;

    private static FaceLandmarker faceLandmarker68;

    private static EyeStateDetector eyeStateDetector;

    private static MaskDetector maskDetector;

    private static GenderPredictor genderPredictor;

    private static QualityOfPoseEx qualityOfPoseEx;

    private static FaceDetector detector;

    private static FaceRecognizer faceRecognizer;

    static {
        LoadNativeCore.LOAD_NATIVE(SeetaDevice.SEETA_DEVICE_AUTO);
        try {
            faceTracker = new FaceTracker(PATH,640,480);
            faceDetector = new FaceDetector(new SeetaModelSetting(-1,new String[]{PATH},SeetaDevice.SEETA_DEVICE_AUTO));
            agePredictor = new AgePredictor(new SeetaModelSetting(COMMON_VALUE_0, AGE_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            faceLandmarker5 = new FaceLandmarker(new SeetaModelSetting(COMMON_VALUE_0, LANDMARKER_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            faceLandmarker68 = new FaceLandmarker(new SeetaModelSetting(COMMON_VALUE_0, LANDMARKER_CSTAS_68, SeetaDevice.SEETA_DEVICE_AUTO));
            eyeStateDetector = new EyeStateDetector(new SeetaModelSetting(-1, EYE_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            maskDetector = new MaskDetector(new SeetaModelSetting(COMMON_VALUE_0, MASK_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            genderPredictor = new GenderPredictor(new SeetaModelSetting(COMMON_VALUE_0, GENDER_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            qualityOfPoseEx = new QualityOfPoseEx(new SeetaModelSetting(COMMON_VALUE_0, POSE_CSTAS, SeetaDevice.SEETA_DEVICE_AUTO));
            detector = new FaceDetector(new SeetaModelSetting(COMMON_VALUE_0, DETECTOR_CSTA, SeetaDevice.SEETA_DEVICE_AUTO));
            faceRecognizer = new FaceRecognizer(new SeetaModelSetting(COMMON_VALUE_0, RECOGNIZER_CSTA, SeetaDevice.SEETA_DEVICE_AUTO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public FaceModel faceTrackAndDetector(FaceModel faceModel) {
        // 人脸数据贴出来，首先获取绘制工具
        // 首先将opencv捕捉到的原始数据帧转为seeta所能识别的数据帧
        BufferedImage imageData = faceModel.getFaceImage();
        SeetaImageData seetaImageData = SeetafaceUtil.toSeetaImageData(imageData);
        // 人脸识别检测
        // 人脸追踪 拿到提取到的人脸追踪数据
        SeetaTrackingFaceInfo[] tracks = faceTracker.Track(seetaImageData);
        // 跟踪到的人脸,提取帧数据中的人脸特征向量数据，封装成一个SeetaRect矩形框对象
        SeetaRect[] detect = faceDetector.Detect(seetaImageData);
        // 遍历识别Seetaface的矩形框对象，映衬在原始数据帧上 有多少个人脸数据就循环多少次进行绘制
        if (tracks.length==detect.length){
            for (int i = 0; i < detect.length; i++) {
                SeetaPointF[] pointFeatures68 = new SeetaPointF[68];
                faceLandmarker68.mark(seetaImageData,detect[i],pointFeatures68);
                // 首先将矩形画出来
                log.info("人脸识别特征矩形数据：x：{}，y：{}，width：{}，height：{}",detect[i].x,detect[i].y,detect[i].width,detect[i].height);
                log.info("人脸追踪数据 score:{},PID:{},frame_no:{},x:{},y:{}",tracks[i].score,tracks[i].PID,tracks[i].frame_no,tracks[i].x,tracks[i].y);
                faceModel.setFrameCount(tracks[i].frame_no);
                imageData = SeetafaceUtil.writeRect(imageData, detect[i]);
                Graphics gra = imageData.getGraphics();
                // 生成画笔
                // 绘制图片RGB数据
                gra.drawImage(imageData, COMMON_VALUE_0, COMMON_VALUE_0, null);
                // 绘制字符串之前设置画笔颜色
                gra.setColor(Color.blue);
                // 绘制字符串
                gra.drawString("PID:"+tracks[i].PID +"-Frame_NO:"+ tracks[i].frame_no+"-Score:"+tracks[i].score,tracks[i].x,tracks[i].y);
                // 然后将五官画出来
                for (SeetaPointF seetaPointF : pointFeatures68) {
                    gra.setColor(Color.red);
                    gra.drawString("·", (int) seetaPointF.getX(), (int) seetaPointF.getY());
                }
                // 关闭绘制
                gra.dispose();
            }
        }
        // 处理完所有特征数据之后，将处理完滕征数据的视频数据帧返回给Canvas
        faceModel.setFaceImage(imageData);
        return faceModel;
    }

    /**
     * 单张人脸信息识别
     *
     * @param faceModel 传入一个视频帧数据
     * @return 返回一个视频帧分析后的模型
     */
    @Override
    public FaceModel analyseFaceFrame(FaceModel faceModel) {
        BufferedImage bufferedImage = faceModel.getFaceImage();
        try {
            SeetaImageData seetaImageData = SeetafaceUtil.toSeetaImageData(bufferedImage);
            SeetaRect[] detects = faceDetector.Detect(seetaImageData);
            faceModel.setFaceTotal(detects.length);
            // 初始化一个年龄对象
            int[] ages = new int[1];
            float[] floats = new float[1];
            boolean wearMaskOrNot = false;
            float[] yaw = new float[1];
            float[] pitch = new float[1];
            float[] roll= new float[1];
            EYE_STATE[] eyeStatus = new EYE_STATE[2];
            GENDER[] gender = new GENDER[1];
            for (SeetaRect seetaRect : detects) {
                // 人脸特征关键向量
                SeetaPointF[] pointFeatures = new SeetaPointF[5];
                // 先用人脸关键点位定位器定位数据点，为人脸预测做准备
                faceLandmarker5.mark(seetaImageData, seetaRect, pointFeatures);
                agePredictor.PredictAgeWithCrop(seetaImageData, pointFeatures, ages);
                eyeStatus = eyeStateDetector.detect(seetaImageData, pointFeatures);
                wearMaskOrNot = maskDetector.detect(seetaImageData, seetaRect, floats);
                genderPredictor.PredictGenderWithCrop(seetaImageData, pointFeatures, gender);
                qualityOfPoseEx.check(seetaImageData,seetaRect,pointFeatures,yaw,pitch,roll);
            }
            faceModel.setAges(ages);
            faceModel.setEyeStatus(eyeStatus);
            faceModel.setWearMaskOrNot(wearMaskOrNot);
            // female 女 male 男
            faceModel.setGender(gender[COMMON_VALUE_0].toString());
            faceModel.setYaw(yaw);
            faceModel.setPitch(pitch);
            faceModel.setRoll(roll);
            faceModel.setPoseResult(analysePose(pitch[COMMON_VALUE_0],yaw[COMMON_VALUE_0],roll[COMMON_VALUE_0]));
            return faceModel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 人体姿评估
     * @param pitch 计算俯仰角
     * @param yaw 计算偏航角
     * @param roll 计算横滚角
     * @return 返回一个人体姿态评估结果
     */
    public static String analysePose(double pitch, double yaw , double roll){
        String pose;
        // 判断人体姿态
        if (pitch > COMMON_VALUE_0_POINT_5 && Math.abs(yaw) < COMMON_VALUE_0_POINT_2 && Math.abs(roll) < COMMON_VALUE_0_POINT_2) {
            pose = "人体向上看，身体直立";
        } else if (pitch < - COMMON_VALUE_0_POINT_5 && Math.abs(yaw) < COMMON_VALUE_0_POINT_2 && Math.abs(roll) < COMMON_VALUE_0_POINT_2) {
            pose = "人体向下看，身体直立";
        } else if (Math.abs(pitch) < COMMON_VALUE_0_POINT_2 && yaw > COMMON_VALUE_0_POINT_5 && Math.abs(roll) < COMMON_VALUE_0_POINT_2) {
            pose = "人体向左看，身体直立";
        } else if (Math.abs(pitch) < COMMON_VALUE_0_POINT_2 && yaw < - COMMON_VALUE_0_POINT_5 && Math.abs(roll) < COMMON_VALUE_0_POINT_2) {
            pose = "人体向右看，身体直立";
        } else if (Math.abs(pitch) < COMMON_VALUE_0_POINT_2 && Math.abs(yaw) < COMMON_VALUE_0_POINT_2 && roll > COMMON_VALUE_0_POINT_5) {
            pose = "人体向右倾斜，身体直立";
        } else if (Math.abs(pitch) < COMMON_VALUE_0_POINT_2 && Math.abs(yaw) < COMMON_VALUE_0_POINT_2 && roll < -COMMON_VALUE_0_POINT_5) {
            pose = "人体向左倾斜，身体直立";
        } else {
            pose = "其他人体姿态";
        }
        return pose;
    }

    /**
     * 人脸特征对比
     *
     * @param faceModel bufferedImage compareFaceImage
     * @return 返回判断两个图像是否相等后的模型
     */
    @Override
    public FaceModel faceRecognizerCompare(FaceModel faceModel) {
        float[] sourceFeature = getFaceFeature(faceModel.getFaceImage());
        float[] targetFeature = getFaceFeature(faceModel.getCompareFaceImage());
        //两个人脸向量做对比，得出分数
        float similarity = faceRecognizer.CalculateSimilarity(sourceFeature, targetFeature);
        log.info("相似度:{}", similarity);
        //如果相似度大于0.6，则认为两个图像相等
        int compare = Float.compare(similarity, 0.600000f);
        if (compare == 0 || compare > 0) {
            faceModel.setIsEqual(Boolean.TRUE);
        } else {
            faceModel.setIsEqual(Boolean.FALSE);
        }
        return faceModel;
    }

    /**
     * 根据传入的缓冲图像生成向量特征数据
     *
     * @param faceImage 缓冲图像
     * @return 图像向量特征
     */
    private float[] getFaceFeature(BufferedImage faceImage) {
        //五点定位中心点
        int size = 5;
        //图像数据
        SeetaImageData imageData = SeetafaceUtil.toSeetaImageData(faceImage);
        SeetaRect[] detect = detector.Detect(imageData);
        SeetaPointF[] pointFS = new SeetaPointF[size];
        int[] mask = new int[size];
        //图像，图像的人脸关键点定位，有多个人脸的情况只取第一个
        faceLandmarker5.mark(imageData, detect[0], pointFS, mask);
        //提取图像向量特征
        float[] feature = new float[faceRecognizer.GetExtractFeatureSize()];
        faceRecognizer.Extract(imageData, pointFS, feature);
        return feature;
    }
}

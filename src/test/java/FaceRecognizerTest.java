import com.seeta.sdk.FaceDetector;
import com.seeta.sdk.FaceLandmarker;
import com.seeta.sdk.FaceRecognizer;
import com.seeta.sdk.SeetaDevice;
import com.seeta.sdk.SeetaImageData;
import com.seeta.sdk.SeetaModelSetting;
import com.seeta.sdk.SeetaPointF;
import com.seeta.sdk.SeetaRect;
import com.seeta.sdk.util.LoadNativeCore;
import com.seeta.sdk.util.SeetafaceUtil;

/**
 * Description: 人脸向量特征测试
 *      源数据（source）：即人脸识别抓取的人脸数据
 *      目标数据（target）：即存储的人脸图像数据
 *
 * @author lyl
 * @since 2023/3/14 15:06
 */
public class FaceRecognizerTest {

    //模型文件的父路径位置
    public static final String BASE_PATH = "D:\\develop\\idea_workspace\\seetaface-java-demo\\src\\main\\resources\\models";

    //人脸对比图片位置
    public static final String TEST_PICTURE = "D:\\face\\11.jpg";

    //模型文件的具体位置
    public static final String[] DETECTOR_CSTA = {BASE_PATH + "/face_detector.csta"};
    public static final String[] LANDMARKER_CSTA = {BASE_PATH + "/face_landmarker_pts5.csta"};
    //人脸向量特征提取和对比模型
    public static final String[] RECOGNIZER_CSTA = {BASE_PATH + "/face_recognizer.csta"};

    static {
        LoadNativeCore.LOAD_NATIVE(SeetaDevice.SEETA_DEVICE_AUTO);
    }

    public static void main(String[] args) {
        try {
            //人脸检测器
            FaceDetector detector = new FaceDetector(new SeetaModelSetting(0, DETECTOR_CSTA, SeetaDevice.SEETA_DEVICE_AUTO));
            //关键点定位器 5点
            FaceLandmarker faceLandmarker = new FaceLandmarker(new SeetaModelSetting(0, LANDMARKER_CSTA, SeetaDevice.SEETA_DEVICE_AUTO));
            //人脸向量特征提取和对比器
            FaceRecognizer faceRecognizer = new FaceRecognizer(new SeetaModelSetting(0, RECOGNIZER_CSTA, SeetaDevice.SEETA_DEVICE_AUTO));
            //获取提取的向量特征大小
            System.out.println(faceRecognizer.GetExtractFeatureSize());
            //第二张图片的位置
            String faceFileName = "D:\\face\\22.jpg";
            //五点定位中心点
            int size = 5;

            //源图像数据
            SeetaImageData sourceImageData = SeetafaceUtil.toSeetaImageData(faceFileName);
            SeetaRect[] sourceDetect = detector.Detect(sourceImageData);
            SeetaPointF[] sourcePointFS = new SeetaPointF[size];
            int[] sourceMask = new int[size];
            //源图像，源图像的人脸关键点定位，有多个人脸的情况只取第一个
            faceLandmarker.mark(sourceImageData, sourceDetect[0], sourcePointFS, sourceMask);
            //提取源图像向量特征
            float[] sourceFeature = new float[faceRecognizer.GetExtractFeatureSize()];
            faceRecognizer.Extract(sourceImageData, sourcePointFS, sourceFeature);
            
            //目标图像数据
            SeetaImageData targetImageData = SeetafaceUtil.toSeetaImageData(TEST_PICTURE);
            SeetaRect[] targetDetect = detector.Detect(targetImageData);
            SeetaPointF[] targetPointFS = new SeetaPointF[size];
            int[] targetMask = new int[size];
            //目标图像，目标图像的人脸关键点定位，有多个人脸的情况只取第一个
            faceLandmarker.mark(targetImageData, targetDetect[0], targetPointFS, targetMask);
            //提取目标图像向量特征
            float[] targetFeature = new float[faceRecognizer.GetExtractFeatureSize()];
            faceRecognizer.Extract(targetImageData, targetPointFS, targetFeature);

            //两个人脸向量做对比，得出分数
            if (sourceFeature != null && targetFeature != null) {
                float similarity = faceRecognizer.CalculateSimilarity(sourceFeature, targetFeature);
                System.out.printf("相似度:%f\n", similarity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

import java.io.File;

/**
 * @author puye(0303)
 * @since 2023/3/10
 */

public class FileTreePrinter {

    public static String getFileTree(String directory, String[] ignoreFolders) {
        StringBuilder sb = new StringBuilder();
        File root = new File(directory);
        if (!root.isDirectory()) {
            return "";
        }
        printDirectoryTree(root, "", sb, ignoreFolders);
        return sb.toString();
    }

    private static void printDirectoryTree(File folder, String indent, StringBuilder sb, String[] ignoreFolders) {
        sb.append(indent).append("+--").append(folder.getName()).append("/").append("\n");
        String[] files = folder.list();
        for (int i = 0; i < files.length; i++) {
            File file = new File(folder, files[i]);
            if (file.isDirectory() && !contains(ignoreFolders, file.getName())) {
                printDirectoryTree(file, indent + "   ", sb, ignoreFolders);
            } else if (file.isFile()) {
                sb.append(indent).append("   |").append("--").append(file.getName()).append("\n");
            }
        }
    }

    private static boolean contains(String[] arr, String targetValue) {
        for (String s : arr) {
            if (s.equals(targetValue)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String[] ignoreFolders = {".idea", "target","test",".git"};
        String fileTree = FileTreePrinter.getFileTree("D:\\myWorkProject\\demoProject\\seetaface-demo", ignoreFolders);
        System.out.println(fileTree);
    }
}
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.VideoInputFrameGrabber;

public class TestCamera {
    public static void main(String[] args) {
        // 获取摄像头设备列表
        FrameGrabber[] cameras = new VideoInputFrameGrabber[4];
        for (int i = 0; i < cameras.length; i++) {
            cameras[i] = new VideoInputFrameGrabber(i);
            try {
                cameras[i].start();
                System.out.println("Camera " + i + " exists.");
            } catch (Exception e) {
                System.out.println("Camera " + i + " does not exist.");
            }
        }
        // 关闭摄像头设备
        for (FrameGrabber camera : cameras) {
            try {
                camera.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
